import { observable, action } from 'mobx';
import axios from 'axios'
import { AsyncStorage } from 'react-native'

const api = axios.create({
    baseURL: 'https://dev.addictivelearning.io/api/v1',
});

class Store {
    //SIGNING UP
    @observable termsOfUse = false;
    @observable passwordError = true;
    @observable passwordLengthError = true;
    @observable emailError = true;
    @observable signUpEmail = '';
    @observable signUpPassword = '';
    @observable confirmPassword = '';
    @observable dataFromStorage = '';

    @action setEmail(email) {
        this.signUpEmail = email;
    }

    @action setPassword(pass) {
        this.signUpPassword = pass;
    }

    @action setConfirmPassword(pass) {
        this.confirmPassword = pass;
    }

    @action switchTermsOfUse() {
        this.termsOfUse = !this.termsOfUse;
    }

    @action passwordValidation() {
        this.signUpPassword.length < 8 || this.signUpPassword === ''
            ? this.passwordLengthError = true
            : this.passwordLengthError = false
    }

    @action passwordConfirmationCheck() {
        this.signUpPassword === this.confirmPassword
            ? this.passwordError = false
            : this.passwordError = true
    }

    @action getDataFromStorage(data) {
        this.dataFromStorage = data
    }


    @action emailValidation() {
        const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

        expression.test(String(this.signUpEmail).toLowerCase()) === false || this.emailError === ''
            ? this.emailError = true
            : this.emailError = false
    }

    @action register = async () => {

        return fetch('https://dev.addictivelearning.io/api/v1/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.signUpEmail,
                password: this.signUpPassword,
                password_confirmation: this.confirmPassword,
                terms_of_use: this.termsOfUse,
            }),
        })
            .then(response => response.json())
            .then(async res => {
                this.userData = res.data.user
                await AsyncStorage.setItem('authStatus', 'true')
            });

    }

    @action currentUser = async () => {
        return fetch('https://dev.addictivelearning.io/api/v1/current', {
            method: 'GET'
        })
            .then(response => response.json())
            .then(res => this.userData = res)
    }

    @action reset() {
        this.isAuthenticated = false;
        this.signInEmail = ''
        this.signInPassword = ''
        this.signUpEmail = ''
        this.signUpPassword = ''
        this.confirmPassword = ''
        this.userData = ''
        this.emailError = true
        this.passwordError = true
        this.passwordLengthError = true
    }

    @action logOut = async () => {
        return fetch('https://dev.addictivelearning.io/api/v1/logout', {
            method: 'POST'
        })
            .then(response => response.json())
            .then(res => console.log(res))
            .then(this.reset())
            .then(await AsyncStorage.setItem('authStatus', 'false'))
    }

    //SIGNING IN
    @observable signInEmail = ''
    @observable signInPassword = ''
    @observable userData = ''
    @observable logInError = false
    @observable loadingStatus = false
    @observable isAuthenticated = false

    @action isEmailEmpty() {
        this.signInEmail === '' ? this.logInError = true : this.logInError = false
    }

    @action isPasswordEmpty() {
        this.signInPassword === '' ? this.logInError = true : this.logInError = false
    }

    @action formCheck() {
        this.isEmailEmpty()
        this.isPasswordEmpty()

        this.logIn()
    }

    @action setSignInEmail(email) {
        this.signInEmail = email;
    }

    @action setSignInPassword(pass) {
        this.signInPassword = pass;
    }

    @action logIn = async () => {
        const data = new FormData();

        data.append('email', this.signInEmail)
        data.append('password', this.signInPassword)

        const requestOptions = {
            method: 'POST',
            body: data,
        };

        return fetch('https://dev.addictivelearning.io/api/v1/login', requestOptions)
            .then(async response =>   {
                response.ok ? this.isAuthenticated = true 
                    : this.logInError = true
            })
    }

}

export default new Store();
