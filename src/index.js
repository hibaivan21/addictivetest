import React from 'react';
import AppNavigation from './navigation';
import { Provider } from "mobx-react";
import Store from './store'

const App = () => (
    <Provider store={Store}>
        <AppNavigation />
    </Provider>
);

export default App;
