import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { inject, observer } from "mobx-react";

import DefaultButton from '../../../components/buttons/DefaultButton'

import styles from './styles'

@inject("store")
@observer
class Settings extends Component {

  handleLogOut = () => {
    this.props.store.logOut()
    this.props.navigation.navigate('SignUp')
  }

  render() {
    return (
      <View style={styles.container}>
        <DefaultButton
          title="<LOG OUT />"
          onPress={() => this.handleLogOut()}
        />
      </View>
    );
  }
}

export default Settings;
