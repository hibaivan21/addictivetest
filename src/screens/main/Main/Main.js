import React, { Component } from 'react';
import { View, Text, Image, AsyncStorage } from 'react-native';
import { inject, observer } from "mobx-react";
import DefaultButton from '../../../components/buttons/DefaultButton'

import styles from './styles'


@inject("store")
@observer
class Main extends Component {
  

  render() {
    console.log(this.props.store.userData)
    return (
      <View style={styles.container}>
      <Text style={{ fontSize: 40, fontWeight: 'bold' }}>Home Screen</Text>
        <Text>{JSON.stringify(this.props.store.userData)}</Text>
      </View>
    );
  }
}

export default Main;