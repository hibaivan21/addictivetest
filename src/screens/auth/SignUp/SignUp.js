import React, { Component } from 'react';
import { View, Text, Alert, Image, AsyncStorage } from 'react-native';
import { inject, observer } from "mobx-react";
import { TouchableOpacity } from 'react-native-gesture-handler';
import { CheckBox } from 'react-native-elements';

import DefaultInput from '../../../components/inputs/DefaultInput'
import DefaultButton from '../../../components/buttons/DefaultButton'

import styles from './styles'


@inject("store")
@observer
class SignUp extends Component {

  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem('authStatus');
      if (value !== null && value === 'true') { 
        this.props.navigation.navigate('App')
      }
    } catch (error) {
      // Error retrieving data
    }
  }

  handleEmailChange = text => this.props.store.setEmail(text);
  handlePasswordChange = text => this.props.store.setPassword(text);
  handleConfirmPasswordChange = text => this.props.store.setConfirmPassword(text);

  register = () => {
    this.props.store.register()
    this.props.navigation.navigate('Welcome');
  }

  handleRegistration = () => {
    const { termsOfUse, emailError, passwordError, passwordLengthError } = this.props.store
    termsOfUse && !emailError && !passwordError && !passwordLengthError
      ? this.register()
      : Alert.alert("check all fields for correctness and agree to the terms of use first")
  }

  render() {
    const { navigation } = this.props
    const { passwordError, passwordLengthError, emailError, termsOfUse } = this.props.store
    return (
      <View style={styles.container}>
        <Image source={require('../../../assets/icon.png')} style={styles.logo} />
        {emailError ? <Text>- valid email address</Text> : null}
        {passwordLengthError ? <Text>- password must be longer than 8 characters</Text> : null}
        {passwordError ? <Text>- passwords must match</Text> : null}
        <View style={styles.inputContainer}>
          <DefaultInput
            placeholder='email'
            value={this.props.store.signUpEmail}
            onChangeText={text => this.handleEmailChange(text)}
            onEndEditing={() => this.props.store.emailValidation()}
          />
          <DefaultInput
            placeholder='password'
            value={this.props.store.signUpPassword}
            onChangeText={text => this.handlePasswordChange(text)}
            onEndEditing={() => this.props.store.passwordValidation()}
            secureTextEntry={true}
          />
          <DefaultInput
            placeholder='confirm password'
            value={this.props.store.confirmPassword}
            onChangeText={text => {
              this.handleConfirmPasswordChange(text),
                this.props.store.passwordConfirmationCheck()
            }}
            secureTextEntry={true}
          />
        </View>
        <View style={styles.bottomTextView}>
          <CheckBox
            checked={termsOfUse}
            containerStyle={{ paddingHorizontal: 0 }}
            size={20}
            checkedColor={'black'}
            uncheckedColor={'black'}
            onPress={() => this.props.store.switchTermsOfUse()}
          />
          <TouchableOpacity onPress={() => this.props.store.switchTermsOfUse()}>
            <Text style={{ color: 'black', fontWeight: 'bold' }}>i agree with the terms of use</Text>
          </TouchableOpacity>
        </View>
        <View>
          <DefaultButton title='Sign Up' onPress={() => this.handleRegistration()} />
          <TouchableOpacity onPress={() => navigation.navigate('LogIn')} style={{ alignItems: 'center' }}>
            <Text style={{ fontSize: 17 }}>{text}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default SignUp

const text = '<Log in if you already have an account />'