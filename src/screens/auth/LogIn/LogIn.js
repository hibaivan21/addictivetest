import React, { Component } from 'react';
import { View, Text, Image, ActivityIndicator, Alert, AsyncStorage } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { inject, observer } from "mobx-react";

import DefaultInput from '../../../components/inputs/DefaultInput'
import DefaultButton from '../../../components/buttons/DefaultButton'

import styles from './styles'

@inject("store")
@observer
class LogIn extends Component {
  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem('authStatus');
      if (value !== null && value === 'true') { 
        this.props.navigation.navigate('App')
      }
    } catch (error) {
      // Error retrieving data
    }
  }

  handleEmailChange = text => this.props.store.setSignInEmail(text);
  handlePasswordChange = text => this.props.store.setSignInPassword(text);

  handleLogIn = () => {
    if (this.props.store.signInEmail === '' || this.props.store.signInPassword === '') {
      Alert.alert("fields cannot be empty") 
    } else this.props.store.formCheck()
  }
  

  render() {
    const { navigation } = this.props
    console.log(this.props.store.isAuthenticated)
    this.props.store.isAuthenticated ? navigation.navigate('Welcome') : null
    return (
      <View style={styles.container}>
        <Image source={require('../../../assets/icon.png')} style={styles.logo} />
        {this.props.store.loadingStatus ?
          <View style={{position: 'absolute'}}><ActivityIndicator size={40} color={'black'} /></View>
          : null}
        {this.props.store.logInError ? <Text>wrong email or password</Text> : null}
        <View style={styles.inputContainer}>
          <DefaultInput
            placeholder='email'
            onChangeText={text => this.handleEmailChange(text)}
            value={this.props.store.signInEmail}
          />
          <DefaultInput
            placeholder='password'
            value={this.props.store.signInPassword}
            onChangeText={text => this.handlePasswordChange(text)}
            secureTextEntry={true}
          />
        </View>
        <View>
          <DefaultButton title='Sign In' onPress={() => this.handleLogIn()} />
          <TouchableOpacity onPress={() => navigation.navigate('SignUp')} style={{ alignItems: 'center' }}>
            <Text style={{ fontSize: 17 }}>{text}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default LogIn

const text = '<Move to registration page />'