import React, { Component } from 'react';
import { View, Text, Image, AsyncStorage } from 'react-native';
import { inject, observer } from "mobx-react";
import { TouchableOpacity } from 'react-native-gesture-handler';

import styles from './styles'

@inject("store")
@observer
class Welcome extends Component {
    async componentDidMount() {
        await AsyncStorage.setItem('authStatus', 'true')
        this.props.store.currentUser()
    }

    render() {
        const { navigation } = this.props
        return (
            <View style={styles.container}>
                <Image source={require('../../../assets/icon.png')} style={styles.logo} />
                <TouchableOpacity
                    style={styles.startButton}
                    onPress={() => navigation.navigate('App')}
                >
                    <Text style={styles.buttonText}>LET'S START</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default Welcome;
