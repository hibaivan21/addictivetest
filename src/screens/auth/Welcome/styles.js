import { StyleSheet, Dimensions } from 'react-native';


const height = Dimensions.get('window').height

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        height: 350,
        width: 350
    },
    welcomeText: {
        fontWeight: '500',
        fontSize: 35
    },
    buttonText: {
        color: 'black',
        fontWeight: '700',
        fontSize: 20,
        textDecorationLine:'underline'
    },
    startButton: {
        height: 150,
        width: 150,
        borderWidth: 4,
        borderColor: 'black',
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
