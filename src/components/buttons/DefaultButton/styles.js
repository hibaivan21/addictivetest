import {StyleSheet, Dimensions} from 'react-native';


export default StyleSheet.create({
  buttonTitle: {
    fontSize: 22,
    color: 'white',
  },
  buttonStyle: {
    backgroundColor: 'black',
    borderRadius: 14,
    height: 42,
    marginHorizontal: '30%',
  },
  buttonContainer: {
    marginVertical: 10,
  },
});
